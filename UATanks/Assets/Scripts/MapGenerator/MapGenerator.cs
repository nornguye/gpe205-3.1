﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class MapGenerator : MonoBehaviour
{
    //Rows
    public int rows;
    //Columns
    public int cols;
    //Width of the room
    private float roomWidth = 60.0f;
    //Height of the room
    private float roomHeight = 60.0f;
    //Room Game Object List
    public List<GameObject> roomPrefab;
    public int seed;
    //Room Grid
    private Room[,] grid;
    //enum list of map types
    public enum mapTypes
    {
        mapOfTheDay,
        seedMap,
        randomMap
    }

    public mapTypes mapSelection;
    void Start()
    {
        GenerateMap();
    }
    public GameObject RandomRoomPrefab()
    {
        return roomPrefab[Random.Range(0, roomPrefab.Count)];
    }
    //Generate Map
    public void GenerateMap()
    {
        //Switch 
        switch (mapSelection)
        {
            //Case 1 = Map of the Day
            case mapTypes.mapOfTheDay:
            {
                DateTime dt = DateTime.Now;
                break;
            }
            //Case 2 = Map of the Day
            case mapTypes.randomMap:
            {
                break;
            }
            //Case 3 = Map of the Day
            case mapTypes.seedMap:
            {
                UnityEngine.Random.InitState(seed);
                break;
            }
        }
        //Grid
        grid = new Room[cols, rows];

        Vector3 centerOffset = new Vector3(roomWidth * (cols - 1) * 0.5f, 0.0f, roomHeight * (rows - 1) * 0.5f);
        //Rows
        for (int i = 0; i < rows; i++)
        {
            //Columns
            for (int j = 0; j < cols; j++)
            {
                //
                float xPosition = roomWidth * j;
                float zPosition = roomHeight * i;
                Vector3 newPosition = new Vector3(xPosition, 0.0f, zPosition);
                //
                newPosition -= centerOffset;

                CreateTile(newPosition, i, j);
                
            }
        }

    }
    //Create the Tiles (Doors, Walls, Barricade)
    void CreateTile(Vector3 position, int i, int j)
    {
        GameObject tempRoomObj = Instantiate(RandomRoomPrefab(), position, Quaternion.identity) as GameObject;

        tempRoomObj.transform.parent = this.transform;

        tempRoomObj.name = "Room_" + j + "," + i;

        Room tempRoom = tempRoomObj.GetComponent<Room>();

        OperateDoors(tempRoom, i, j);

        grid[j, i] = tempRoom;
    }
    //Operating doors
    void OperateDoors (Room room, int i, int j)
    {
        //****North and South Door****
        if (i == 0)
        {
            room.doorNorth.SetActive(false);
        }
        else if (i == rows - 1)
        {
            room.doorSouth.SetActive(false);
        }
        else
        {
            room.doorNorth.SetActive(false);
            room.doorSouth.SetActive(false);
        }
        //****North and South Door****
        //****East and West Door****
        if (j == 0)
        {
            room.doorEast.SetActive(false);
        }
        else if (j == cols - 1)
        {
            room.doorWest.SetActive(false);
        }
        else
        {
            room.doorEast.SetActive(false);
            room.doorWest.SetActive(false);
        }
        //****East and West Door****
    }
    //Map of the Day
    public int mapDayMthTime(DateTime dateToUse)
    {
        return dateToUse.Year + dateToUse.Month + dateToUse.Day + dateToUse.Hour + dateToUse.Minute + dateToUse.Second + dateToUse.Millisecond;
    }
}
