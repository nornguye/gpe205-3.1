﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpSpawner : MonoBehaviour
{
    //Object Prefab
    public GameObject objectSpawned;
    //Object Prefab
    public GameObject spawnedObject;
    //Object Prefab
    private Transform tf;
    //Object Prefab
    public float timeBetweenSpawns;
    //Object Prefab
    public float spawnTimer;
    //Object Prefab
    public Transform[] spawnLocations;

    void Start()
    {
        tf = GetComponent<Transform>();

        spawnTimer = timeBetweenSpawns;
    }

    void Update()
    {
        if (spawnedObject == null)
        {
            spawnTimer -= Time.deltaTime;

            if (spawnTimer <= 0)
            {
                int locationId = Random.Range(0, spawnLocations.Length);

                Transform spawnTransform = spawnLocations[locationId];

                spawnedObject = Instantiate(objectSpawned, spawnTransform.position, spawnTransform.rotation) as GameObject;

                spawnTimer = timeBetweenSpawns;
            }
        }
    }
}
