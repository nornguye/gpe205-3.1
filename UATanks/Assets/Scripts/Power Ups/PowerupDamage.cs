﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupDamage : Powerup {

    public float bonusAmount;
    // Use this for initialization
    public override void PowerUpOff(TankData data)
    {

        base.PowerUpOff(data);
        data.bullet.bulletDamage += bonusAmount;
    }

    public override void PowerUpOn(TankData data)
    {
        base.PowerUpOn(data);
        data.bullet.bulletDamage -= bonusAmount;
    }
}
