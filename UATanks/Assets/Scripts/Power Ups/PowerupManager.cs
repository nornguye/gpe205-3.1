﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupManager : MonoBehaviour {
    public List<Powerup> powerups;

    private TankData data;
    // Use this for initialization
    void Start ()
    {
        data = GetComponent<TankData>();
    }
	
	// Update is called once per frame
	void Update ()
	{
	    List<Powerup> powerupsToRemove = new List<Powerup>();
	    foreach (Powerup powerup in powerups)
	    {
	        powerup.timer -= Time.deltaTime;
	        if (powerup.timer <= 0)
	        {
	            powerupsToRemove.Add(powerup);
	        }
	    }

	    foreach (Powerup powerup in powerupsToRemove)
	    {
	        RemovePowerup(powerup);
	    }
	}

    public void AddPowerup(Powerup powerupToAdd)
    {
        Powerup newPowerup = new Powerup();
        if (powerupToAdd.powerupsTypes == Powerup.PowerUpsType.HealthPowerUp)
        {
            newPowerup = new PowerupHealth((PowerupHealth)powerupToAdd);
        }
    }
    public void RemovePowerup(Powerup powerupToRemove)
    {

    }
}
