﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupHealth : Powerup {

    public float healthBonus;
	public override void PowerUpOn(TankData data)
    {
        base.PowerUpOn(data);
        data.health.currentHealth += healthBonus;
    }
    public override void PowerUpOff(TankData data)
    {
        base.PowerUpOff(data);
        data.health.currentHealth -= healthBonus;
    }
}
