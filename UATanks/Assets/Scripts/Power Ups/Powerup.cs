﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//System.serialize is use to display the variables in the inspector
[System.Serializable]
public class Powerup : MonoBehaviour
{
    public float timer; //Timer
    public bool isPerm;

    public PowerUpsType powerupsTypes;
    public virtual void PowerUpOn(TankData data)
    { }
    public virtual void PowerUpOff(TankData data)
    { }
}

