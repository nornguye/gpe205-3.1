﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickups : MonoBehaviour
{
    public PowerupHealth healthPowerup;
    public PowerupSpeed speedPowerup;
    public PowerupDamage damagePowerup;

    Transform tf;

	// Use this for initialization
	void Start ()
	{
	    
	}
	
    public void OnTriggerEnter(Collider other)
    {
        PowerupManager pm = other.GetComponent<PowerupManager>();
        if (pm != null)
        {
            pm.AddPowerup(powerup);
        }
        Destroy(gameObject);
    }
    
    public void OnDestroy()
    {

    }
}
