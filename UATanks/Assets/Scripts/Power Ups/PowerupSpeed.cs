﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupSpeed : Powerup {

    public float bonusAmount;
    // Use this for initialization
    public override void PowerUpOff(TankData data)
    {

        base.PowerUpOff(data);
        data.moveSpeed += bonusAmount;
    }

    public override void PowerUpOn(TankData data)
    {
        base.PowerUpOn(data);
        data.moveSpeed -= bonusAmount;
    }
}
